package com.hmall.user;


import com.hmall.api.config.DefaultFeignConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan("com.hmall.user.mapper")
@EnableFeignClients(defaultConfiguration = {DefaultFeignConfig.class})
public class userServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(userServiceApplication.class,args);
    }
}
