package com.hmall.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HmallGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(HmallGatewayApplication.class,args);
    }
}
